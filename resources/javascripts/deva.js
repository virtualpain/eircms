$(".sub-main-nav ").hide();
$(".main-nav-cat").hide();
$(".post-advanced").hide();
$("#debug").hide();
var currentID="";
var theID="";
var openNav=false;
$(".open-menu").click(function(event){
	event.preventDefault();
	theID = $(this).data('nav');
	if (currentID !== theID) {
		$(".sub-main-nav").slideUp('fast',function(){
			$(".main-nav-cat").hide();
			$("#mainnav-"+theID).show();
			$(this).slideDown('fast');
			currentID = theID;
			openNav=true;
		});
	}
	else {
		$(".sub-main-nav").slideUp('fast');
		$(".main-nav-cat").hide();
		openNav=false;
		currentID="";
	}
});

// $("a").click(function(event){event.preventDefault();});

if ($(".alert-flash").is(":visible"))
{
	setTimeout(function(){
		$(".alert-flash").slideUp("slow");
	},5000);
}

$(".post-advanced-button").click(function(event){
	event.preventDefault();
	$(".post-advanced").toggle("fast");
})
$("#toggleDebug").click(function(){
	$("#debug").toggle("fast");
});