<div class="content">
	<div class="row-fluid">
		<div class="span12">
			<h3>Categories</h3>
			<p>Manage your categories</p>
			<?php echo form_open("admin/category_new",array("class"=>"form-inline")); ?>
				<input type="text" class="span3" name="name" placeholder="New category">
				<input type="text" class="span3" name="slug" placeholder="Slug url">
				<input type="text" class="span3" name="description" placeholder="Description">
				<button class="btn btn-primary" type="submit">Create</button>
			</form>
			<?php if ($categories->num_rows() > 0): ?>
			<ul class="list list-categories">
				<?php foreach ($categories->result() as $category): ?>
				<li class="category"><strong><a href="javascript:;"><?php echo $category->name ?></a></strong> &mdash; <?php echo $category->description ?></li>
				<?php endforeach; ?>
			</ul>
			<?php else: ?>
			<p>There are no categories</p>
			<?php endif; ?>
		</div>
	</div>
</div>