		<footer>
			<div class="row-fluid">
				<div class="span12">
					<a href="<?php echo urlto('http://eirworks.net/eircms',TRUE); ?>">support</a>
					|
					<a href="<?php echo urlto('mailto:hello@eirworks.net',TRUE); ?>">feature request</a>
					|
					custom built for <?php echo $company_name ?>
					|
					generated in: {elapsed_time}
					<?php if (ENVIRONMENT == "development"): ?>
					|
					<a href="javascript:;" id="toggleDebug">Debug</a>
					<?php endif; ?>
				</div>
			</div>
		</footer>
		<?php if (ENVIRONMENT == "development"): ?>
		<div class="row-fluid">
			<div class="span12">
				<div id="debug">
					<h4>Debug</h4>
					<ul>
						<li><strong>Route</strong>:
							<ul>
								<li><strong>Controller</strong>: <?php echo $this->uri->rsegment(1); ?></li>
								<li><strong>Method</strong>: <?php echo $this->uri->rsegment(2); ?></li>
							</ul>
						</li>
						<li><strong>Session</strong>:
							<ul>
								<?php foreach ($this->session->all_userdata() as $key => $value): ?>
									<li><strong><?php echo $key ?></strong>: <?php echo $value; ?></li>
								<?php endforeach ?>
							</ul>
						</li>
						<li><strong>URI</strong>:
							<ul>
								<li><strong>Current URL</strong>: <?php echo current_url() ?></li>
								<li><strong>URI String</strong>: <?php echo uri_string() ?></li>
							</ul>
						</li>
						<li><strong>Speed</strong>: {elapsed_time}</li>
					</ul>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<script type="text/javascript" src="<?php echo getResource('deva','js') ?>"></script>
</body>
</html>