<!doctype html>

<html>
<head>
	<title>Register</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php echo getCSS('styles.css') ?>
</head>
<body>
	<div class="container">
		<div class="row-fluid">
			<div class="span4 offset4">
				<h3>Register</h3>
				<!-- start session warnings -->
				<?php if ($this->session->flashdata('error')): ?>
				<div class="alert alert-error"><?php echo $this->session->flashdata('error'); ?></div>
				<?php elseif ($this->session->flashdata('success')): ?>
				<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
				<?php endif; ?>
				<!-- end session warning -->
				<!-- start validation error -->
				<?php if (validation_errors()): ?>
				<div class="alert alert-error"><?php echo validation_errors(); ?></div>
				<?php endif; ?>
				<!-- end validation error -->
				<?php echo form_open('auth/register') ?>
					<p><input class="span12" type="text" name="username" value="<?php echo set_value('username') ?>"  placeholder="username"></p>
					<p><input class="span12" type="email" name="email" value="<?php echo set_value('email') ?>"  placeholder="email"></p>
					<p><input class="span12" type="password" name="password" placeholder="password"></p>
					<p><input class="span12" type="password"  name="password_check" 	placeholder="password again"></p>
					<p>
						<button class="btn btn-primary" type="submit">Register</button>
						<a href="<?php echo urlto() ?>" class="btn">cancel</a>
					</p>
					<p><a href="<?php echo urlto('login') ?>">Already have an account?</a></p>
				</form>
			</div>
		</div>
	</div>
</body>
</html>