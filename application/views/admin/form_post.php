<div class="content">
	<div class="row-fluid">
		<div class="span12">
			<h2>New Post</h2>
			<p><a href="<?php echo urlto('admin') ?>">&larr; back to admin</a></p>
			<!-- validation errors -->
			<?php if (validation_errors()): ?>
			<div class="alert alert-error">
				<?php echo validation_errors(); ?>
			</div>
			<?php endif; ?>
			<!-- end of validation errors -->
			<?php if ($this->uri->rsegment(2) == "action_post_new" || $this->uri->rsegment(2) == "action_post_create"): ?>
			<?php echo form_open('admin/post/create',array("class"=>"form-horizontal")); ?>
			<?php elseif ($this->uri->rsegment(2) == "action_post_edit" || $this->uri->rsegment(2) == "action_post_update"): ?>
			<?php echo form_open("admin/post/update/".$post->id,array("class"=>"form-horizontal")); ?>
			<?php endif; ?>
				<p><input type="text" name="title" class="span12" placeholder="Post title" value="<?php echo (isset($post) ? $post->title : "") ?>"></p>
				<p><textarea name="content" id="" cols="30" rows="10" class="span12" placeholder="Post content"><?php echo (isset($post) ? $post->content : "") ?></textarea></p>
				<div class="control-group">
					<label for="category_id" class="control-label">Category:</label>
					<div class="controls">
						<select name="category_id" id="category_id">
							<?php foreach ($categories->result() as $category): ?>
								<option <?php echo (isset($post) ? ($post->category_id == $category->id ? "selected" : "") : "") ?> value="<?php echo $category->id ?>"><?php echo $category->name ?></option>
							<?php endforeach ?>
						</select>
						<!-- or
						<input type="text" name="new_category" placeholder="Create new category"> -->
					</div>
				</div>
				<div class="post-advanced">
					<div class="control-group">
						<label for="slug" class="control-label">url:</label>
						<div class="controls"><input type="text" name="slug" value="<?php echo (isset($post) ? $post->slug : ""); ?>"></div>
					</div>
				</div>
				<div class="form-action">
					<?php if (isset($post) && $post->is_published == 1): ?>
					<button type="submit" name="submit" value="publish" class="btn btn-primary">Update</button>
					<?php else: ?>
					<button type="submit" name="submit" value="publish" class="btn btn-primary">Publish</button>
					<button type="submit" name="submit" value="save" class="btn">Save as draft</button>
					<?php endif; ?>
					<a href="<?php echo urlto('admin'); ?>" class="btn">cancel</a>
					<a href="javascript:;" class="btn post-advanced-button">advanced</a>
				</div>
			</form>
		</div>
	</div>
</div>