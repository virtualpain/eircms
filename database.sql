--
-- MySQL 5.5.29
-- Sun, 10 Mar 2013 00:22:14 +0000
--

CREATE TABLE `categories` (
   `id` int(11) not null auto_increment,
   `name` varchar(255),
   `user_id` int(11),
   `is_default` tinyint(4) default '0',
   `slug` varchar(255),
   `description` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

INSERT INTO `categories` (`id`, `name`, `user_id`, `is_default`, `slug`, `description`) VALUES 
('1', 'uncategorized', '0', '1', 'uncategorized', 'Uncategorized posts or pages');

CREATE TABLE `comments` (
   `id` int(11) not null auto_increment,
   `user_id` int(11),
   `user_email` varchar(255),
   `title` varchar(255),
   `content` text,
   `created` datetime,
   `updated` datetime,
   `post_id` int(11),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `comments` is empty]

CREATE TABLE `config` (
   `id` int(11) not null auto_increment,
   `name` varchar(64),
   `value` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=10;

INSERT INTO `config` (`id`, `name`, `value`) VALUES 
('1', 'default_results', '10'),
('2', 'site_name', 'ACME'),
('3', 'site_slogan', ''),
('4', 'default_theme', 'default'),
('5', 'enable_register', '1'),
('6', 'site_company', 'ACME Inc.'),
('7', 'homepage_id', '0'),
('8', 'enable_forum', '0'),
('9', 'enable_gallery', '0');

CREATE TABLE `files` (
   `id` int(11) not null auto_increment,
   `name` varchar(255),
   `user_id` int(11),
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `files` is empty]

CREATE TABLE `posts` (
   `id` int(11) not null auto_increment,
   `title` varchar(255),
   `content` text,
   `category_id` int(11),
   `created` datetime,
   `updated` datetime,
   `user_id` int(11),
   `is_published` tinyint(4) default '1',
   `is_page` tinyint(4) default '0',
   `slug` varchar(255),
   `is_deleted` tinyint(4) default '0',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `posts` is empty]

CREATE TABLE `users` (
   `id` int(11) not null auto_increment,
   `username` varchar(16),
   `email` varchar(255),
   `fullname` varchar(255),
   `password` varchar(255),
   `created` datetime,
   `role` varchar(20),
   `description` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- [Table `users` is empty]