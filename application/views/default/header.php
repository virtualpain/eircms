<!doctype html>

<html>
<head>
	<title><?php echo $site_name; ?></title>
	<?php echo getCSS('styles.css'); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<div class="container">
		<header>
			<div class="row-fluid">
				<div class="span12">
					<h1><?php echo $site_name; ?></h1>
				</div>
			</div>
		</header>
		<?php if ($this->session->flashdata('error')): ?>
		<div class="alert alert-flash alert-error"><?php echo $this->session->flashdata('error')  ?></div>
		<?php elseif ($this->session->flashdata('success')): ?>
		<div class="alert alert-flash alert-success"><?php echo $this->session->flashdata('success') ?></div>
		<?php elseif ($this->session->flashdata('info')): ?>
		<div class="alert alert-flash alert-info"><?php echo $this->session->flashdata('info'); ?></div>
		<?php endif; ?>