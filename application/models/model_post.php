<?php 


class Model_post extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function post_new($data=array())
	{
		$this->db->insert("posts",$data);
	}

	public function update($id,$data)
	{
		$this->db->where("id",$id);
		$this->db->update("posts",$data);
	}

	// get posts
	// parameter:
	// - number : limit/number of results
	// - start  : start from id
	// - is_published: NULL-> all, TRUE->only published, FALSE->only unpublished 
	public function posts($is_published=NULL,$number=10,$start=0)
	{
		if (is_null($is_published))
		{
			$result = $this->db->get("posts",$number,$start);
		}
		else
		{
			$result = $this->db->get_where("posts",array("is_published"=>($is_published == TRUE ? 1 : 0)),$number,$start);
		}
		return $result;
	}
}