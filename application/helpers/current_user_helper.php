<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists("current_user"))
{
	function current_user($key="username")
	{
		$CI =& get_instance();

		if ($CI->session->userdata('current_user'))
		{
			$user = $CI->db->get_where('users',array("username"=>$CI->session->userdata('current_user')))->row_array();
			return $user[$key];
		}
		else
		{
			return FALSE;
		}
	}
}