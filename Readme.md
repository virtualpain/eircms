# eirCMS

Simple CMS based on CodeIgniter.

## Installing

* Put all files inside a codeigniter folder, merge/replace all things
* Change the Environment in index.php to "PRODUCTION"
* Change application/config/production/ database files
* Put it on server, and show it up in the browser!
* Register an account! First account will automatically become admin.